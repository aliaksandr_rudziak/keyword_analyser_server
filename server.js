/**
 * Created by alex_rudyak on 2/17/15.
 */
//#!/usr/bin/env node

var app = require('./app');

app.set('port', 3000);
app.set('host_address', "localhost");

var server = app.listen(app.get('port'), app.get('host_address'), function() {
    console.info('Keyword-Analyser Express server listening on port ' + server.address().port);
});