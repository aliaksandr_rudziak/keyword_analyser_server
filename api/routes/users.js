/**
 * Created by alex_rudyak on 2/17/15.
 */

var router = require('express').Router();
var users = require('../controllers/index').userController;

router.get('/:id', users.getUser);
router.post('', users.createUser);
router.put('/:id', users.updateUser);

module.exports = router;