/**
 * Created by alex_rudyak on 2/17/15.
 */

module.exports = function(app) {
    app.get('/', require('../controllers').homeController.welcome);
    app.use('/user', require('./users'));
    app.use('/transactions', require('./transactions'));
    app.use('/products', require('./products'));
    app.use('/converter', require('./converter'));
};