/**
 * Created by alex_rudyak on 2/17/15.
 */

var router = require('express').Router();
var products = require('../controllers/index').productsController;

router.get('', products.getAllProductIds);
router.post('', products.addProductWithDomain);
router.delete('', products.removeProductWithDomain);

module.exports = router;
