/**
 * Created by alex_rudyak on 3/5/15.
 */

var router = require('express').Router();
var converter = require('../controllers').converterController;

router.get('/', converter.welcomeToConverter);
router.get('/documents', converter.getConvertedDocuments);
router.post('/documents', converter.postConvertDocument);
router.get('/status/:document', converter.getCheckConversionDocumentStatus);

module.exports = router;