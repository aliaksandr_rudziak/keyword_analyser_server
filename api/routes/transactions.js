/**
 * Created by alex_rudyak on 2/17/15.
 */

var router = require('express').Router();
var transactions = require('../controllers/index').transactionsController;

router.get('/:transactionId', transactions.getTransaction);
router.post('', transactions.createTransaction);
router.put('/:transactionId', transactions.updateTransaction);
router.get('', transactions.getAllTransactions);

module.exports = router;
