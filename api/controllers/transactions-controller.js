/**
 * Created by alex_rudyak on 2/17/15.
 */

var Q = require('q');
var _ = require('lodash');
var formidable = require('formidable');
var mongoose = require('mongoose');
var notificationSender = require('./../../core/notification-sender');
var utilities = require('./../../core/utilities');

module.exports = {
    getTransaction: function(req, res) {
        var transactionId = req.params.transactionId;
        req.mongodb.models.Transaction.findById(transactionId).exec()
            .then(function(transaction) {
                res.json(transaction);
            })
            .then(null, function(err) {
                res.status(400).json(err);
            });
    },

    createTransaction: function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            var transaction = new req.mongodb.models.Transaction({
                status: fields.status,
                price: fields.price,
                quantity: fields.quantity,
                pages: fields.pages,
                client_id: fields.client_id,
                transaction_id: fields.transaction_id,
                _id: new mongoose.Types.ObjectId
            });

            utilities.uploadAttachment(files, transaction.client_id, function(attachmentName) {
                transaction.attachment = attachmentName;

                req.mongodb.models.Transaction.create(transaction)
                    .then(function(transaction) {
                        notifyUserAboutTransaction(req, req.mongodb, transaction);

                        res.status(201).json(transaction);
                    })
                    .then(null, function(err) {
                        res.status(500).json(err);
                    });
            });
        });
    },

    updateTransaction: function(req, res) {
        var transactionId = req.params.transactionId;
        req.mongodb.models.Transaction.findById(transactionId).exec()
            .then(function(transaction) {
                transaction.status = req.body.status;
                transaction.transaction_id = req.body.transactionId;

                return transaction.save();
            })
            .then(function(transaction) {
                notifyUserAboutTransaction(req, req.mongodb, transaction);

                res.json(transaction);
            })
            .then(null, function(err) {
                res.status(400).json(err);
            });
    },

    getAllTransactions: function(req, res) {
        req.mongodb.models.Transaction.find().exec()
            .then(function(transactions) {
                res.json(transactions);
            })
            .then(null, function() {
                res.status(500).json(err);
            });
    }
};

function notifyUserAboutTransaction(req, db, transaction) {
    if (transaction.status === 'purchased') {
        db.models.User.findById(transaction.client_id).exec()
            .then(function(user) {
                notificationSender.sendNotification(user, {
                    type: "product-purchase",
                    status: "purchased",
                    quantity: transaction.quantity,
                    price: transaction.price * transaction.quantity,
                    pages: transaction.pages,
                    attachment: "http://grodno.instinctools.ru:" + req.app.get('port') + "/attachments" + transaction.attachment
                });
            })
            .then(null, function(err) {
                console.error(err);
            });
    }
}
