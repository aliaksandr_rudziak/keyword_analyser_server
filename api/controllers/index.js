/**
 * Created by alex_rudyak on 2/17/15.
 */

module.exports = {
    homeController: require('./home-controller'),
    productsController: require('./products-controller'),
    userController: require('./user-controller'),
    transactionsController: require('./transactions-controller'),
    converterController: require('./converter-controller')
};