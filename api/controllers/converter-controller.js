/**
 * Created by alex_rudyak on 3/5/15.
 */

var Converter = require('./../../core/converter');
var util = require('util');
var formidable = require('formidable');
var fileUtilities = require('./../../core/utilities');
var path = require('path');
var Q = require('q');
var _ = require('lodash');

var ConversionStatus = {
    CONVERSION_COMPLETED: "C",
    CONVERSION_PROCESSING: "P"
};

var documentConverter = new Converter();

documentConverter
    .on('ready', function() {
        "use strict";
        console.info('Conversion session ready to use');
    })
    .on('closed', function() {
        "use strict";
        console.info('Conversion session was stopped');
    })
    .on('conversion-done', function(filename) {
        "use strict";

        console.info("Document conversion completed! `" + filename + "` is ready to download.");
    })
    .on('conversion-error', function(error) {
        "use strict";
        console.info("Document conversion completed with error: " + util.inspect(error) + ".");
    });

documentConverter.startConverter();

/*
 File Structure Manager
 */
var _filestructureManager;
function filestructureManager() {
    "use strict";
    return _filestructureManager;
}
function setFilestructureManager(filestructureManager) {
    "use strict";
    _filestructureManager = filestructureManager;
    fileUtilities.useFilestructureManager(_filestructureManager);
}

module.exports = {
    welcomeToConverter: function(req, res) {
        setFilestructureManager(req.app.get('file-structure-manager'));

        res.send("this is converter from one of open specification documents to pdf.");
    },

    getConvertedDocuments: function(req, res) {
        setFilestructureManager(req.app.get('file-structure-manager'));

        var userId = req.body.userId;
        if (!userId) {
            return res.status(401).json({
                message: "Need user to receive list of converted documents"
            });
        }

        req.mongodb.models.User.findById(userId).exec()
            .then(function(user) {
                res.status(200).json(user.converted_files);
            })
            .then(null, function(err) {
                res.status(500).json(err);
            });
    },

    postConvertDocument: function(req, res) {
        "use strict";
        setFilestructureManager(req.app.get('file-structure-manager'));

        if (!(req.headers['content-type'] && req.headers['content-type'].match(/multipart/i))) {
            return res.status(400).json({
                message: "You should upload document with request"
            });
        }

        Q.Promise(function(resolve, reject) {
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
                "use strict";
                if (err) {
                    return reject({
                        status: 400,
                        error: err
                    });
                }

                if (!files.attachment) {
                    return reject({
                        status: 400,
                        error: {
                            message: "You should attach document for conversion"
                        }
                    });
                }

                var userId = fields.userId;
                if (!userId) {
                    return reject({
                        status: 401,
                        error: {
                            message: "Need user to receive list of converted documents"
                        }
                    });
                }

                req.mongodb.models.User.findById(userId).exec()
                    .then(function(user) {
                        if (!user) {
                            return reject({
                                status: 401,
                                error: {
                                    message: util.format('No user with such id %s', userId)
                                }
                            });
                        }

                        resolve({
                            user: user,
                            files: files
                        });
                    })
                    .then(null, function(err) {
                        reject({
                            status: 401,
                            error: err
                        });
                    });
            });
        })
            .then(function(options) {
                "use strict";
                return Q.Promise(function(resolve, reject) {
                    fileUtilities.uploadConvertAttachment(options.files, options.user._id.toString(), function(convertFilename) {
                        if (!convertFilename) {
                            return reject({
                                message: "Some error occurred when uploading file for conversion"
                            });
                        }

                        options.convertFilename = convertFilename;
                        resolve(options);
                    });
                });
            })
            .then(function(options) {
                "use strict";



                var convertedFilename = path.join('/', path.basename(options.convertFilename, path.extname(options.convertFilename)) + ".pdf");
                options.user.converted_files.push({
                    file: convertedFilename,
                    status: ConversionStatus.CONVERSION_PROCESSING
                });
                options.user.save();

                res.status(200).json({
                    "status": ConversionStatus.CONVERSION_PROCESSING,
                    "documentId": path.basename(convertedFilename, '.pdf')
                });

                return Q(options);
            })
            .then(function(options) {
                "use strict";

                return documentConverter.enqueueDocument(path.join(filestructureManager().conversionsFolder(), options.convertFilename), filestructureManager().attachmentsFolder())
                    .then(function(convertedFilename) {
                        var fileIndex = _.findIndex(options.user.converted_files, { file: convertedFilename });
                        options.user.converted_files[fileIndex].status = ConversionStatus.CONVERSION_COMPLETED;
                        return options.user.save();
                    })
                    .fail(function(err) {
                        console.error(err);
                    })
                    .done();
            })
            .then(null, function(err) {
                if (err.error) {
                    res.status(err.status || 500).json(err.error);
                } else {
                    res.status(500).json(err);
                }
            });
    },

    getCheckConversionDocumentStatus: function(req, res) {
        "use strict";
        setFilestructureManager(req.app.get('file-structure-manager'));

        var documentId = req.params.document;
        var userId = req.body.userId;
        if (!userId) {
            return res.status(401).json({
                message: "Need user to receive list of converted documents"
            });
        }

        req.mongodb.models.User.findById(userId).exec()
            .then(function(user) {
                if (!user) {
                    var error = new Error("No user found with such id", userId);
                    error.status = 401;
                    return Q.reject(error);
                }

                var foundConversionDocument = _.find(user.converted_files, function(doc) {
                    return _.startsWith(path.basename(doc.file), documentId);
                });

                if (foundConversionDocument) {
                    return res.status(200).json({
                        "status": foundConversionDocument.status,
                        "documentId": documentId,
                        "document": util.format("/%s.pdf", documentId)
                    });
                } else {
                    return res.status(400).json({
                        message: "Not found document with such id"
                    });
                }
            })
            .then(null, function(err) {
                res.status(err.status || 500).json(err);
            });
    }
};