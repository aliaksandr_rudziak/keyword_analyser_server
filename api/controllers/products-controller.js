/**
 * Created by alex_rudyak on 2/17/15.
 */

var Q = require('q');
var _ = require('lodash');
var mongoose = require('mongoose');

module.exports = {
    getAllProductIds: function(req, res) {
        req.mongodb.models.Product.find().exec()
            .then(function(products) {
                return Q(_.map(products, function(product) {
                    return product.product_domain_id;
                }));
            })
            .then(function(productsDomains) {
                res.json(productsDomains);
            })
            .then(null, function(err) {
                res.status(500).json(err);
            });
    },

    addProductWithDomain: function(req, res) {
        var productDomain = req.body.productDomain;
        req.mongodb.models.Product.create({ product_domain_id: productDomain, _id: new mongoose.Types.ObjectId })
            .then(function(product) {
                res.status(201).json(product);
            })
            .then(null, function(err) {
                res.status(500).json(err);
            });
    },

    removeProductWithDomain: function(req, res) {
        var productDomain = req.body.productDomain;

        req.mongodb.models.Product.remove({ product_domain_id: productDomain }).exec()
            .then(function(products) {
                res.status(204).end();
            })
            .then(null, function(err) {
               res.status(500).json(err);
            });
    }
};