/**
 * Created by alex_rudyak on 2/17/15.
 */

var Q = require('q');
var _ = require('lodash');
var mongoose = require('mongoose');

module.exports = {
    getUser: function(req, res) {
        var userId = req.params.id;
        req.mongodb.models.User.findById(userId).exec()
            .then(function(user) {
                res.json(user);
            })
            .then(null, function(err) {
                res.status(400).json(err);
            });
    },

    createUser: function(req, res) {
        req.mongodb.models.User.create({ _id: new mongoose.Types.ObjectId })
            .then(function(user) {
                res.status(201).json(user);
            })
            .then(null, function(err) {
                res.status(500).json(err);
            });
    },

    updateUser: function(req, res) {
        var userId = req.params.id;
        req.mongodb.models.User.findById(userId).exec()
            .then(function(user) {
                user.email = req.body.email;
                user.receipt = req.body.receipt;

                return user.save();
            })
            .then(function(user) {
                res.status(201).json(user);
            })
            .then(null, function(err) {
                res.status(400).json(err);
            });
    }
};