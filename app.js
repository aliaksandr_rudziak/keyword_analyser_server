/**
 * Created by alex_rudyak on 2/17/15.
 */

'use strict';

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var database = require('./database');


var app = express();
app.disable('view engine');
app.disable('views');

app.set('file-structure-manager', require('./core/filestructure-manager'));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

if (process.env.NODE_ENV === 'development') {
    app.use(require('errorhandler')({ dumpExceptions: true, showStack: true }));
}

app.use(function(req, res, next) {
    res.set({
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST, GET, PUT, DELETE, OPTIONS",
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
    });
    next();
});

app.use(function(req, res, next) {
    database(function(err, connection) {
        if (err) {
            return next(err);
        }

        req.mongodb = connection;
        next();
    });
});

require('./api/routes')(app);
app.use('/attachments', express.static(__dirname + '/public/attachments'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var error = new Error("Not found");
    error.status = 404;
    next(error);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        err.status = err.status || 500;
        res.status(err.status).json(err);
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    err.status = err.status || 500;
    res.status(err.status).json(err);

});

module.exports = app;