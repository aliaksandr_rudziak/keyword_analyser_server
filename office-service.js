/**
 * Created by alex_rudyak on 3/10/15.
 */
//#!/usr/local/env node

var exec = require('child_process').exec;
var nconf = require('nconf');

nconf.args().
    env();

var officeBin = nconf.get('office');
var command = officeBin + " --headless --nofirststartwizard  --\"accept=socket,host=127.0.0.1,port=2002;urp;ServiceManager\"";
exec(command, function(err) {
    "use strict";

    if (err) {
        console.error(err);
    }
});
