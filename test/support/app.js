/**
 * Created by alex_rudyak on 3/6/15.
 */

module.exports = function() {
    "use strict";

    var app = require('../../app');
    app.set('file-structure-manager', require('./fs-utils'));

    app.set('port', 3000);
    app.set('host_address', "localhost");

    var server = app.listen(app.get('port'), app.get('host_address'), function() {
        console.info('[TEST] Keyword-Analyser Express server listening on port ' + server.address().port);
    });

    return server;
};