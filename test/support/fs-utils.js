/**
 * Created by alex_rudyak on 3/6/15.
 */

var fs = require('fs');
var _ = require('lodash');
var path = require('path');

module.exports.clearDirSync = function clearDirSync(dir) {
    "use strict";

    var convertedFiles = fs.readdirSync(dir);
    _.forEach(convertedFiles, function(file) {
        fs.unlinkSync(path.join(dir, file));
    });
};

module.exports.makeDirSync = function makeDirSync(dir) {
    "use strict";

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
};

module.exports.conversionsFolder = function() {
    "use strict";

    return './test/test-conversions';
};
module.exports.attachmentsFolder = function() {
    "use strict";

    return './test/test-attachments';
};