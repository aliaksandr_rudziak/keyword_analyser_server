/**
 * Created by alex_rudyak on 3/9/15.
 */

var database = require('../../database');
var mongoose = require('mongoose');
var Q = require('q');

module.exports = function(cb) {
    "use strict";

    database(function(err, connection) {
        "use strict";

        var _user;

        var helperApi = {};
        helperApi.UserHelper = {
            getUser: function() {
                return Q(_user);
            },

            createUser: function() {
                return Q.Promise(function(resolve, reject) {
                    connection.models.User.create({
                        _id: new mongoose.Types.ObjectId,
                        email: "alesanro@instinctools.ru"
                    })
                        .then(function(user) {
                            _user = user;
                            resolve(_user);
                        })
                        .then(null, function(err) {
                            reject(err);
                        });
                });
            },

            deleteUser: function() {
                return Q.Promise(function(resolve, reject) {
                    if (!_user) {
                        var user = _user;
                        _user = undefined;
                        return resolve(user);
                    }

                    resolve(_user.remove());
                });
            }
        };

        cb(helperApi);
    });
};
