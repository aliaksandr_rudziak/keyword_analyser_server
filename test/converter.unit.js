/**
 * Created by alex_rudyak on 3/5/15.
 */

var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var should = require('should');
var Converter = require('../core/converter');
var fsUtils = require('./support/fs-utils');

describe("Converter to PDF", function() {
    "use strict";
    var converter;
    var testDir = __dirname + "/conversion-test/";
    var sourceDir = __dirname + "/support/resources/";

    before(function() {
        converter = new Converter();
        fsUtils.makeDirSync(testDir);
    });

    after(function() {
        converter = null;
        fsUtils.clearDirSync(testDir);
        fs.rmdirSync(testDir);
    });

    it('should exist', function() {
        should.exist(converter);
    });

    it('should implement eventEmitter', function(done) {
        converter.on('test', function(event) {
            done();
        });
        converter.emit('test', "good");
    });

    describe("when start or stop", function() {
        before(function() {
            converter.stopConverter();
        });

        after(function() {
            //converter.stopConverter();
        });

        afterEach(function() {
            converter.removeAllListeners();
            converter.stopConverter();
        });

        it("should emit `ready` on start", function(done) {
            converter.on('ready', function() {
                done();
            });
            converter.startConverter();
        });

        it('should emit `ready` only once per session', function(done) {
            var emitCount = 0;
            converter.on('ready', function() {
                emitCount++;

                var t = setInterval(function() {
                    clearInterval(t);

                    if (emitCount > 1) {
                        return done(new Error('`ready` invokes more than once'));
                    } else {
                        return done();
                    }
                }, 100);
            });
            converter.startConverter();
            converter.startConverter();
        });

        it("should emit `closed` on stop", function(done) {
            converter.on('closed', function() {
                done();
            });
            converter.startConverter();
            converter.stopConverter();
        });

        it('should emit `closed` only once per session', function(done) {
            var emitCount = 0;
            converter.on('closed', function() {
                emitCount++;

                var t = setInterval(function() {
                    clearInterval(t);

                    if (emitCount > 1) {
                        return done(new Error('`closed` invokes more than once'));
                    } else {
                        return done();
                    }
                }, 100);
            });
            converter.startConverter();
            converter.stopConverter();
            converter.stopConverter();
        });

        it('should emit `conversion-done` on successful conversion', function(done) {
            var sourceFile = _.last(fs.readdirSync(sourceDir));
            var sourceFilepath = path.join(sourceDir, sourceFile);
            converter.on('conversion-done', function() {
                done();
            });
            converter.startConverter();
            converter.enqueueDocument(sourceFilepath, testDir);
        });
    });

    describe("when making conversion", function() {

        var sourceFiles;
        before(function() {
            sourceFiles = fs.readdirSync(sourceDir);
        });

        beforeEach(function() {
            converter.startConverter();
        });

        afterEach(function() {
            fsUtils.clearDirSync(testDir);
            converter.stopConverter();
        });

        it('sources should exist', function() {
            should.exist(sourceFiles);
            should(sourceFiles).be.Array;
            sourceFiles.length.should.be.above(2);
        });

        it('should convert single file without concurrent', function() {
            var sourceFile = _.last(sourceFiles);
            var sourceFilepath = path.join(sourceDir, sourceFile);
            var promise = converter.enqueueDocument(sourceFilepath, testDir);
            promise.should.be.Promise;
            promise.should.be.fulfilled;

            return promise;
        });

        it('should have right-formatted result path', function(done) {
            var sourceFile = _.last(sourceFiles);
            var sourceFilepath = path.join(sourceDir, sourceFile);
            converter.startConverter();
            converter.enqueueDocument(sourceFilepath, testDir)
                .then(function(result) {
                    should(result).not.be.empty;
                    result.should.startWith('/');
                    result.should.endWith('.pdf');
                    result.should.not.match(function(item) {
                        return item.indexOf(testDir) > -1;
                    });

                    done();
                });
        });

        it('should queuing multiple files one a time', function(done) {
            var queuedDocumentsSequence = [];
            _.forEach(sourceFiles, function(file, idx) {
                var sourceFilepath = path.join(sourceDir, file);
                converter.enqueueDocument(sourceFilepath, testDir)
                    .then(function() {
                        queuedDocumentsSequence.push(idx);

                        if ((idx + 1) === sourceFiles.length) {
                            checkSequence();
                        }
                    })
                    .fail(function(err) {
                        done(err);
                    });
            });

            function checkSequence() {
                var sampleSequence = _.map(sourceFiles, function(a, idx) {
                    return idx;
                });
                queuedDocumentsSequence.should.match(sampleSequence);

                fs.readdir(testDir, function(err, files) {
                    if (err) {
                        return done(err);
                    }

                    should.exist(files);
                    files.length.should.be.equal(sourceFiles.length);
                    _.forEach(files, function(file) {
                        path.extname(file).should.be.equal('.pdf');
                    });

                    done();
                });
            }
        });
    });
});
