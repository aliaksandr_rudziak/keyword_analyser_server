/**
 * Created by alex_rudyak on 3/6/15.
 */

var util = require('util');
var should = require('should');
var request = require('supertest');
var fs = require('fs');
var path = require('path');
var fsUtils = require('./support/fs-utils');
var databaseHelperInitializer = require('./support/database-helper');

describe('API Conversion service', function() {
    "use strict";

    var app;
    var agent;
    var existedUser;
    var notExistedUserId;
    var databaseHelper;

    var sourceDir = __dirname + "/support/resources";
    var sourceFiles;

    before(function(done) {
        app = require('./support/app')();
        agent = request.agent(app);
        notExistedUserId = "540000000000000000000000";

        fsUtils.makeDirSync(fsUtils.attachmentsFolder());
        fsUtils.makeDirSync(fsUtils.conversionsFolder());

        databaseHelperInitializer(function(helperAPI) {
            databaseHelper = helperAPI;
            databaseHelper.UserHelper.createUser()
                .then(function(user) {
                    existedUser = user;
                    done();
                })
                .then(null, function(err) {
                    done(err);
                });
        });

        sourceFiles = fs.readdirSync(sourceDir);
    });

    after(function(done) {
        app.close();
        agent = null;

        fsUtils.clearDirSync(fsUtils.attachmentsFolder());
        fs.rmdirSync(fsUtils.attachmentsFolder());
        fsUtils.clearDirSync(fsUtils.conversionsFolder());
        fs.rmdirSync(fsUtils.conversionsFolder());

        databaseHelper.UserHelper.deleteUser()
            .then(function() {
                existedUser = null;
                done()
            })
            .then(null, function(err) {
                done(err);
            });
        databaseHelper = null;
    });

    it('should exist', function() {
        should.exist(app);
        should.exist(agent);
    });

    describe('/converter', function() {
        it("GET should 200", function(done) {
            agent
                .get('/converter')
                .expect(200)
                .end(done);
        });

        describe('/documents', function() {
            var route = '/converter/documents';
            it('GET should be an error without user 401', function(done) {
                agent
                    .get(route)
                    .expect(401)
                    .end(done);
            });

            it('GET should be an error with not existed user 500', function(done) {
                agent
                    .get(route)
                    .send({ userId: notExistedUserId })
                    .expect(500)
                    .end(done);
            });

            it('GET should return array of converted documents 200', function(done) {
                agent
                    .get(route)
                    .send({ userId: existedUser._id })
                    .expect(200)
                    .expect(function(res) {
                        should(res.body).be.Array;
                    })
                    .end(done);
            });

            it("POST should be an error without attached document 400", function(done) {
                agent
                    .post(route)
                    .expect(400)
                    .expect(function(res) {
                        should(res.body).be.an.Object;
                        res.body.should.have.property('message');
                    })
                    .end(done);
            });

            it("POST should be an error without user 401", function(done) {
                agent
                    .post(route)
                    .attach('attachment', path.join(sourceDir, sourceFiles[3]))
                    .expect(401)
                    .expect(function(res) {
                        should(res.body).be.an.Object;
                        res.body.should.have.property('message');
                    })
                    .end(done);
            });

            it("POST should be an error with not existed user 401", function(done) {
                agent
                    .post(route)
                    .field("userId", notExistedUserId )
                    .attach('attachment', path.join(sourceDir, sourceFiles[3]))
                    .expect(401)
                    .expect(function(res) {
                        should(res.body).be.an.Object;
                        res.body.should.have.property('message');
                    })
                    .end(done);
            });

            it("POST should be a status of conversion (pending) 200", function(done) {
                agent
                    .post(route)
                    .field("userId", existedUser._id.toString())
                    .attach('attachment', path.join(sourceDir, sourceFiles[3]))
                    .expect(200)
                    .expect(function(res) {
                        should(res.body).be.an.Object;
                        res.body.should.have.property('status');
                        res.body.status.should.be.equal('P');
                        res.body.should.have.property('documentId');
                        should(res.body.documentId).not.be.empty;
                    })
                    .end(done);
            });
        });

        describe('/status', function() {
           var route = '/converter/status';
            var notExistedDocumentId = 'not_existed_document_id';

            function makeRouteWithId(id) {
                return util.format('%s/%s', route, id);
            }

            it('GET should be an error without userId 401', function(done) {
                agent
                    .get(makeRouteWithId(notExistedDocumentId))
                    .expect(401)
                    .end(done);
            });

            it('GET should be an error for not existed userId 401', function(done) {
                agent
                    .get(makeRouteWithId(notExistedDocumentId))
                    .send({ userId: notExistedUserId })
                    .expect(401)
                    .end(done);
            });

            it('GET should be an error for not existed document 400', function(done) {
                agent
                    .get(makeRouteWithId(notExistedDocumentId))
                    .send({ userId: existedUser._id.toString() })
                    .expect(400)
                    .expect(function(res) {
                        should.exist(res.body);
                        res.body.should.have.property('message');
                        should(res.body.message).not.be.empty;
                    })
                    .end(done);
            });

            it('GET should be `completed` status for newly created document 200', function(done) {
                agent
                    .post('/converter/documents')
                    .field('userId', existedUser._id.toString())
                    .attach('attachment', path.join(sourceDir, sourceFiles[3]))
                    .end(function(err, res) {
                        if (err) {
                            return done(err);
                        }

                        var documentId = res.body.documentId;

                        var t = setTimeout(function() {
                                agent
                                    .get(makeRouteWithId(documentId))
                                    .send({ userId: existedUser._id })
                                    .expect(200)
                                    .expect(function(res) {
                                        should.exist(res.body);
                                        res.body.should.have.property('document');
                                        path.basename(res.body.document).should.startWith(documentId);
                                        res.body.status.should.be.equal('C');
                                    })
                                    .end(function() {
                                        clearTimeout(t);
                                        done();
                                    });
                        }, 1000);
                    });
            });
        });
    });
});