/**
 * Created by alex_rudyak on 2/17/15.
 */

var mongoose = require('mongoose');
var _ = require('lodash');

var mongooseConnected = false;
var connectionObj;

module.exports = function(cb) {
    if (!mongooseConnected) {
        mongoose.connect('mongodb://localhost/keyword-analyser-test');
    } else {
        if (_.isFunction(cb)) {
            cb(null, connectionObj);
        }
    }

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function (callback) {
        // yay!
        console.info('Connection to database successful!');
        mongooseConnected = true;

        if (_.isFunction(cb)) {
            connectionObj = {
                db: mongoose.connection,
                models: require('./models')
            };

            cb(null, connectionObj);
        }
    });
};