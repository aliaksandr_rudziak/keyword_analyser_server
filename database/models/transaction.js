/**
 * Created by alex_rudyak on 2/17/15.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var transactionSchema = Schema({
    _id: ObjectId,
    pages: String,
    price: Number,
    quantity: Number,
    status: String,
    client_id: ObjectId,
    transaction_id: String,
    transaction_date: Date,
    attachment: String
});

var Transaction = mongoose.model('Transaction', transactionSchema);

module.exports = Transaction;