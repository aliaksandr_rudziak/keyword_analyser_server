/**
 * Created by alex_rudyak on 2/17/15.
 */

module.exports = {
    Product: require('./product'),
    Transaction: require('./transaction'),
    User: require('./user')
};