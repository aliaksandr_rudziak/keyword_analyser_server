/**
 * Created by alex_rudyak on 2/17/15.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var userSchema = Schema({
    _id: ObjectId,
    email: String,
    receipt: String,
    converted_files: [Schema.Types.Mixed]
});

var User = mongoose.model('User', userSchema);

module.exports = User;