/**
 * Created by alex_rudyak on 2/17/15.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var productSchema = Schema({
    _id: ObjectId,
    product_domain_id: String
});

var Product = mongoose.model('Product', productSchema);

module.exports = Product;