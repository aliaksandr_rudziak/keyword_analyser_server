/**
 * Created by alex_rudyak on 3/10/15.
 */

var fs = require('fs');
var path = require('path');
var Q = require('q');
var _ = require('lodash');
var moment = require('moment');

var configPath = './cleaner.config.json';

var Time = {
    SECOND: 1000,
    MINUTE: 1000 * 60,
    HOUR: 1000 * 60 * 60,
    DAY: 1000 * 60 * 60 * 24
};

/**
 * One day (24 hour)
 * @type {Number}
 */
var intervalTime = Time.DAY;
var excludeFiles = ['.presence'];

var timer = setInterval(function() {
    "use strict";

    var config = JSON.parse(fs.readFileSync(configPath).toString('utf8'));
    console.info(require('util').inspect(config));

    /*
        Date will be as boundary for removing files
     */
    var today = moment().subtract(config['timer']['duration'], config['timer']['units']);

    /*
        Start cleaners for watched folders
     */
    var cleaners = _.map(config["watchFolders"], function(folder) {
        return Q.nfcall(fs.readdir, folder)
            .then(_.curry(checkFiles)(folder));
    });

    Q.all(cleaners)
        .then(function() {
            console.info(require('util').inspect(arguments));
        })
        .fail(function(err) {
            console.error(err);
        })
        .done(function() {
            var nextDate = new Date();
            nextDate.setTime(nextDate.getTime() + intervalTime);
            console.info('Cleaner done its work. The next start will planned on ' + nextDate);
        });

    function checkFiles(folder, entries) {
        var statsPromises = [];
        var filesDict = {};

        /*
            Fetch files stats descriptors (async)
         */
        _.forEach(entries, function (entry, idx) {
            statsPromises.push(Q.nfcall(fs.stat, path.join(folder, entry)));
            filesDict[idx] = path.join(folder, entry);
        });

        return Q.all(statsPromises)
            .then(function(stats) {
                var removedFiles = [];
                _.forEach(stats, function(stat, idx) {
                    /*
                        Skip file names contained in `excludeFiles` array
                     */
                    if (_.find(excludeFiles, function(item) {
                            return _.endsWith(filesDict[idx], item);
                        })) {
                        return;
                    }

                    /*
                        Remove only files which creation date is in the past for some period (async)
                     */
                    if (stat.isFile()) {
                        var createTime = moment(stat.ctime);
                        if (today.isAfter(createTime)) {
                            removedFiles.push(Q.nfcall(fs.unlink, filesDict[idx]).thenResolve(filesDict[idx]));
                        }
                    }
                });

                return Q.all(removedFiles);
            });
    }
}, intervalTime);