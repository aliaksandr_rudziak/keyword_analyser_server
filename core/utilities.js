/**
 * Created by alex_rudyak on 2/17/15.
 */

var fs = require('fs');
var path = require('path');

var _filestructureManager = require('./filestructure-manager');

module.exports.uploadAttachment = function(files, userId, callback) {
    uploadAttachmentToFolder(_filestructureManager.attachmentsFolder(), files, callback);
};

module.exports.uploadConvertAttachment = function(files, userId, callback) {
    uploadAttachmentToFolder(_filestructureManager.conversionsFolder(), files, callback);
};

module.exports.useFilestructureManager = function(filestructureManager) {
    "use strict";
    _filestructureManager = filestructureManager;
};

function uploadAttachmentToFolder(folder, files, callback) {
    "use strict";

    if (!files.attachment) {
        if (callback) {
            callback(null);
        }
        return;
    }

    var tmp_path = files.attachment.path;
    // set where the file should actually exists - in this case it is in the "images" directory
    var target_name = "/" + randomString(6*32) + path.extname(files.attachment.name);
    var target_path = folder + target_name;
    // move the file from the temporary location to the intended location
    fs.rename(tmp_path, target_path, function(err) {
        if (err) throw err;
        // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
        fs.unlink(tmp_path, function() {
            if (err) throw err;
            console.info('File uploaded to: ' + target_path + ' - ' + files.attachment.size + ' bytes');
        });

        if (callback) {
            callback(target_name);
        }
    });
}

function randomString(bits) {
    var chars, rand, i, ret;

    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-';
    ret = '';

    // in v8, Math.random() yields 32 pseudo-random bits (in spidermonkey it gives 53)
    while (bits > 0) {
        // 32-bit integer
        rand = Math.floor(Math.random() * 0x100000000);
        // base 64 means 6 bits per character, so we use the top 30 bits from rand to give 30/6=5 characters.
        for (i = 26; i > 0 && bits > 0; i -= 6, bits -= 6) {
            ret += chars[0x3F & rand >>> i];
        }
    }

    return ret;
}