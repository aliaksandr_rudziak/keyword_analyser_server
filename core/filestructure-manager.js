/**
 * Created by alex_rudyak on 3/6/15.
 */

module.exports.attachmentsFolder = attachmentsFolder;
module.exports.conversionsFolder = conversionsFolder;

function attachmentsFolder() {
    return './public/attachments';
}

function conversionsFolder() {
    return './public/convert-documents';
}
