/**
 * Created by alex_rudyak on 3/5/15.
 */

var exec = require('child_process').exec;
var eventEmitter = require("events").EventEmitter;
var path = require('path');
var Q = require('q');
var util = require('util');
var _ = require('lodash');

var libreOfficePython = "/Applications/LibreOffice.app/Contents/MacOS/python";
var documentConverterBin = "/../bin/DocumentConverter.py";

/**
 * Converter implement queue for converting documents with help of LibreOffice.
 * It uses 'DocumentConverter.py' script to perform requests to office service.
 * Converter implements EventEmitter prototype with the next events:
 * - `ready` - when queue started
 * - `closed` - when queue stopped
 * - `conversion-done` - when document conversion was done successfully
 * - `conversion-error` - when conversion command failed or other reasons
 */
function Converter(interpreter) {
    eventEmitter.call(this);

    this._queue = [];
    this._lock = false;
    this._loopId = null;
    this._interpreter = interpreter || libreOfficePython;
}

Converter.prototype = _.create(eventEmitter.prototype);
Converter.prototype.startConverter = function() {
    "use strict";
    var _this = this;

    if (_this._loopId) {
        return;
    }

    _this._loopId = setInterval(function() {
        if (_this._lock) {
            return;
        }
        _this._lock = true;

        if (_.size(_this._queue) === 0) {
            _this._lock = false;
            return;
        }

        var unit = _.first(_this._queue);
        _this._queue = _.rest(_this._queue);

        unit.handle()
            .then(function(filename){
                _this.emit('conversion-done', filename);
            })
            .then(null, function(err) {
                _this._queue.push(unit);
                _this.emit('conversion-error', err);
            })
            .done(function() {
                _this._lock = false;
            });
    }, 250);

    _this.emit('ready', this);
};
Converter.prototype.stopConverter = function() {
    "use strict";
    var _this = this;

    if (_this._loopId) {
        clearInterval(_this._loopId);
        _this._lock = false;
        _this.queue = [];
        _this._loopId = undefined;
        _this.emit('closed');
    }
};
Converter.prototype.enqueueDocument = function enqueueDocument(source, destinationPath) {
    "use strict";

    var _this = this;
    return Q.Promise(function(resolve, reject) {
        _this._queue.push({
            handle: function() {
                return Q.Promise(function(resolve1, reject1) {
                    var futureFilename = "/" + path.basename(source, path.extname(source)) + ".pdf";
                    var futurePath = path.join(destinationPath, futureFilename);
                    var command = util.format("%s %s %s %s", _this._interpreter, __dirname + documentConverterBin, source, futurePath);
                    exec(command, function(err) {
                        if (err) {
                            reject(err);
                            reject1(err);
                        } else {
                            resolve(futureFilename);
                            resolve1(futureFilename);
                        }
                    });
                });
            }
        });
    });
};

module.exports = Converter;