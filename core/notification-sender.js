/**
 * Created by alex_rudyak on 2/17/15.
 */
var api_key = 'key-55a4c9c508fa81c55e9d5c8e340042c4';
var domain = 'sandboxf07e6cf2b67a48d0923733455dad2734.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

var emailConfig = {
    "host": "smtp.mailgun.org",
    "port": "465",
    "from": "Keyword Analyser Support <postmaster@sandboxf07e6cf2b67a48d0923733455dad2734.mailgun.org>",
    "username": "postmaster@sandboxf07e6cf2b67a48d0923733455dad2734.mailgun.org",
    "password": "59264722fd560cf4624b436d0b48b2f5"
};

var sendSMTP = function (receiver, message) {
    var mailOptions = {
        from: emailConfig.from,
        to: "<" + receiver + ">",
        subject: "Keyword Analyser",
        html: "<pre>" + message + "</pre></h4>"
    };

    mailgun.messages().send(mailOptions, function(err, info) {
        if (err) {
            return console.error(err);
        }
        console.log("Message sent" + info.response);
    });
};

module.exports.sendNotification = function(user, data) {
    var message = "Hi, user " + user._id + "!</br> "
        + "You've just bought a translation of " + data.pages + " (" + data.quantity + " items) for $" + data.price + "." +
        " You can also download the <a href=\"" + data.attachment + "\">attachment</a>.</br>" +
        "<h3>Have a nice day!</h3>";
    sendSMTP(user.email, message);
};